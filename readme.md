# matrix.actor
The [matrix.actor](https://matrix.actor/) website, is similar to
[https://matrix.to](https://matrix.to), and aims to offer matrix users
a place to link-to and customize, for displaying a user, room or space
"profile". It currently does not aim at providing a list of clients to
open the matrix content, but to offer a clear and customizable web
entry point to a matrix actor (via matrix.to & other community tools).

## Project
Javascript web-components to convert a `@username:domain.tld`
[matrix.org](https://matrix.org/) username grammar, to a user profile
link, and eventually rooms, spaces, or events.

It tries to have a clean URL, so it can be accessible easily from a
browser.

Also to aims at proving an interface for a matrix user to customize
their profile, based on the values provided by the [public user
profile](https://spec.matrix.org/v1.7/client-server-api/#profiles).

According to the Matrix specification, these keys can be an arbitrary
`key:value` JSON structure.

## Ojectives
Maybe could be improved with displaying:
- avatar
- topic/description
- custom user defined data
- markdown/html content
- widgets
- let user organize and style content logically and visually
- featured rooms

## Development
This project uses javascript web-components and depends on
[sctlib/mwc](https://gitlab.com/sctlib/mwc) to communicate with
matrix servers.

To run a local development server: `npm run dev` (requires latest
`node` version).

## Discussion
Comments, bug reports, ideas always welcome. Join the rooms in the
[#matrix-actor:matrix.org](https://matrix.to/#/#matrix-actor:matrix.org)
space, or open gitlab issues.

## Flags
Test features in the labs, as URL search params in the hash router.

```text
matrix.actor/@user:server.tld#rtc
matrix.actor/@user:server.tld#rtc&other-hash-param=true
```

- `rtc` to show a `rtc-user` from the
  [https://gitlab.com/sctlib/rtc](@sctlib/rtc) web component, under a
  matrix-actor-profile, to connect with this user with web
  RTCPeerConnection
- `auth` to force display a matrix-auth component, event if the no
  "registered user" is logged in (could be via rtc-user).
