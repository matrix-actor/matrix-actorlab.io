import mwc from "../libs/mwc.js";

const DEFAULT_USERNAME = "@username:example.org";

export default class MatrixActorProfile extends HTMLElement {
	/* props */
	get profileId() {
		return this.getAttribute("profile-id") || "@user:example.org";
	}
	get flagRtc() {
		return this.getAttribute("flag-rtc") === "true";
	}

	/* mapping to @sctlib/rtc(-user) input attributes URLSearchParams */
	get matrixPeers() {
		return this.hasAttribute("matrix-peers")
				 ? this.getAttribute("matrix-peers")
							 .split(",")
							 .map((id) => id.trim())
				 : null;
	}
	get signalingMethods() {
		return this.hasAttribute("signaling-methods")
				 ? this.getAttribute("signaling-methods").split(",")
				 : null;
	}

	/* lifecycle */
	connectedCallback() {
		this._render();
	}
	_render() {
		this.replaceChildren();
		const $doms = this._createProfileDoms({
			profileId: this.profileId,
			matrixPeers: this.matrixPeers,
			signalingMethods: this.signalingMethods
		});
		this.append(...$doms);
	}
	_createProfileDoms({ profileId, matrixPeers, signalingMethods}) {
		const $matriToProfileLink = this._createMatrixToLink(profileId);
		const $matrixProfile = this._createMatrixProfile(profileId);
		const $rtcUser = this._createRtcUser({
			profileId,
			matrixPeers,
			signalingMethods
		});
		$matriToProfileLink.replaceChildren($matrixProfile);

		const $doms = [];
		$doms.push($matriToProfileLink);
		if (this.flagRtc) {
			$doms.push($rtcUser);
		}
		return $doms;
	}
	_createMatrixToLink(profileId) {
		const $link = document.createElement("a");
		$link.setAttribute("href", `https://matrix.to/#/${profileId}`);
		$link.setAttribute("rel", "noreferer noopener");
		$link.textContent = profileId;
		return $link;
	}
	_createMatrixProfile(userId) {
		const $profile = document.createElement("matrix-profile");
		$profile.setAttribute("user-id", userId);
		return $profile;
	}
	_createRtcUser({profileId, matrixPeers, signalingMethods}) {
		const $rtcUser = document.createElement("rtc-user");
		if (matrixPeers) {
			$rtcUser.setAttribute("matrix-peers", JSON.stringify([...matrixPeers]));
		} else {
			$rtcUser.setAttribute("matrix-peers", JSON.stringify([profileId]));
		}
		if (signalingMethods) {
			$rtcUser.setAttribute(
				"signaling-methods",
				JSON.stringify([...signalingMethods]),
			);
		} else {
			$rtcUser.setAttribute(
				"signaling-methods",
				JSON.stringify(["matrix-user-device"]),
			);
		}
		return $rtcUser;
	}
}
