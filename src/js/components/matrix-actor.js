import mwc from "../libs/mwc.js";

export default class MatrixActor extends HTMLElement {
	get origin() {
		return this.getAttribute("origin") || new URL(window.location.orgin).origin;
	}
	get hostname() {
		return (
			this.getAttribute("hostname") || new URL(window.location.origin).hostname
		);
	}
	parseUrl() {
		/* use the has router only, except for the matrix parts;
			 @TODO: how to handle roomAlias? */
		const searchParams = new URLSearchParams(window.location.hash.slice(1));

		/* @TODO?: roomAlias, roomId, eventId (or just in libli, and on profiles) */
		const userIdParam =
			window.location.pathname.split("/").find((part) => {
				return part.startsWith("@");
			}) || searchParams.get("user-id");
		const { profileId: userId } = userIdParam ? mwc.api.checkMatrixId(userIdParam) : {};

		return {
			searchParams,
			userId,
		};
	}

	setDocumentTitle() {
		if (this.profileId) {
			document.title = this.profileId;
		} else {
			document.title = this.hostname;
		}
	}
	connectedCallback() {
		const { searchParams, userId } = this.parseUrl();
		this.flagRtc = searchParams.has("rtc");
		this.flagAuth = searchParams.has("auth");
		this.matrixPeers = searchParams.get("matrix-peers");
		this.signalingMethods = searchParams.get("signaling-methods");
		this.profileId = userId;
		this.setDocumentTitle();
		this._render();
	}
	_render() {
		this.replaceChildren();
		let $doms = this.profileId
			? this._createProfileDoms()
			: this._createHomeDoms();
		this.append(...$doms);
	}
	_createHomeDoms() {
		const $doms = [];
		const $menu = document.createElement("matrix-actor-menu");
		const $form = document.createElement("matrix-actor-form");

		$doms.push(...[$form, $menu]);
		if (mwc.api.auth || this.flagAuth) {
			const $auth = document.createElement("matrix-auth");
			$auth.setAttribute("show-user", true);
			$auth.setAttribute("show-guest", true);
			$doms.push($auth);
		}
		return $doms;
	}
	_createProfileDoms() {
		const $profile = document.createElement("matrix-actor-profile");
		$profile.setAttribute("profile-id", this.profileId);
		this.flagRtc && $profile.setAttribute("flag-rtc", this.flagRtc);
		this.matrixPeers && $profile.setAttribute("matrix-peers", this.matrixPeers);
		this.signalingMethods && $profile.setAttribute("signalingmethods", this.signalingMethods);
		return [$profile];
	}
}
