import mwc from "../libs/mwc.js";
import i18n from "../libs/i18n.js";

const t = await i18n.getFile(import.meta.url);

export default class MatrixActorForm extends HTMLElement {
	onSubmit(event) {
		event.preventDefault();
		event.stopPropagation();
		const formData = new FormData(event.target);
		const userId = formData.get("user_id");
		const { user, host, homeserver } = mwc.api.checkMatrixId(userId);
		if (user && host) {
			const matrixUserId = mwc.api.buildProfileAddress("@", user, host);
			const currentURL = new URL(window.location.href);
			currentURL.pathname = `/${matrixUserId}`;
			window.location.href = currentURL.href;
		} else {
			this._showErrorFeedback(t.errors.profile_format);
		}
	}

	_showErrorFeedback(message) {
		const errorElement = document.createElement("output");
		errorElement.textContent = message;
		this.appendChild(errorElement);
		setTimeout(() => {
			this.removeChild(errorElement);
		}, 5000);
	}

	connectedCallback() {
		this._render();
	}
	_render() {
		this.replaceChildren();
		const $form = this._createForm();
		$form.addEventListener("submit", this.onSubmit.bind(this));
		this.append($form);
	}

	_createForm() {
		const $form = document.createElement("form");
		const $fieldset = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		$legend.setAttribute("title", t.legend.title);
		$legend.textContent = t.legend.text;

		const $input = document.createElement("input");
		$input.setAttribute("title", t.input.title);
		$input.placeholder = t.input.placeholder;
		$input.type = "username";
		$input.name = "user_id";
		$fieldset.append($legend, $input);
		$form.append($fieldset);

		return $form;
	}
}
