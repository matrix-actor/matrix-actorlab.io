import i18n from "../libs/i18n.js";

const t = await i18n.getFile(import.meta.url);

const LINKS = [
	{
		href: "https://gitlab.com/matrix-actor",
		text: t.links.repo.text,
		title: t.links.repo.title,
	},
	{
		href: "https://matrix.org/try-matrix/",
		text: t.links.register.text,
		title: t.links.register.title,
	},
];

export default class MatrixActorMenu extends HTMLElement {
	static get observedAttributes() {
		return ["origin"];
	}
	get origin() {
		return this.getAttribute("origin") || window.location.origin;
	}
	connectedCallback() {
		this.render();
	}
	render() {
		const $links = LINKS.map(({ text, href, title }) => {
			const $link = document.createElement("a");
			$link.setAttribute("title", title);
			$link.setAttribute("href", href);
			$link.textContent = text;
			return $link;
		});
		const $listItems = $links.map(($link) => {
			const $li = document.createElement("li");
			$li.append($link);
			return $li;
		});

		const $menu = document.createElement("menu");
		$menu.append(...$listItems);
		this.replaceChildren($menu);
	}
}
