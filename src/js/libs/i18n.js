class I18N {
	/* public API */
	get contentPathname() {
		return "/content/i18n/";
	}
	set contentPathname(basePath) {
		return basePath;
	}
	get srcPathname() {
		return "/src/js/";
	}
	set srcPathname(basePath) {
		return basePath;
	}
	get contentExt() {
		return "json";
	}
	constructor({ contentPathname, srcPathname } = {}) {
		this.contentPathname = contentPathname || this.contentPathname;
		this.srcPathname = srcPathname || this.srcPathname;
	}

	/* public methods */
	/* get translation key */
	t(key) {
		return key;
	}
	/* get translation file from source file */
	async getFile(metaUrl) {
		const translations = await this.getTranslationFile(metaUrl);
		if (translations) {
			return translations;
		} else {
			return this.getTranslationFileFallback(metaUrl);
		}
	}
	async getTranslationFileFallback(metaUrl) {}
	async getTranslationFile(metaUrl) {
		const translationFilePath = this.srcUrlToTranslationsUrl(metaUrl);
		if (!translationFilePath) return;
		try {
			const file = await fetch(translationFilePath).then(async (res) => {
				return await res.json();
			});
			return file;
		} catch (e) {
			console.info("Error fetchig i18n file", e);
		}
	}
	srcUrlToTranslationsUrl(metaUrl) {
		const url = new URL(metaUrl);
		const { pathname } = url;
		try {
			const srcFile = pathname.split(this.srcPathname)[1];
			const srcFileName = srcFile.split(".")[0];
			const contentFilePath = `${this.contentPathname}${srcFileName}.${this.contentExt}`;
			const url = new URL(contentFilePath, import.meta.url);
			return url.href;
		} catch (e) {
			console.info("Could not convert translation file path", e);
			return;
		}
	}
}

const i18n = new I18N();

export { i18n as default, I18N };
