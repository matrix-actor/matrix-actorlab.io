import mwc from "./libs/mwc.js";
import "./libs/rtc.js";

import MatrixActor from "./components/matrix-actor.js";
import MatrixActorForm from "./components/matrix-actor-form.js";
import MatrixActorMenu from "./components/matrix-actor-menu.js";
import MatrixActorProfile from "./components/matrix-actor-profile.js";

const componentDefinitions = {
	"matrix-actor": MatrixActor,
	"matrix-actor-form": MatrixActorForm,
	"matrix-actor-menu": MatrixActorMenu,
	"matrix-actor-profile": MatrixActorProfile,
};

if (typeof window !== "undefined") {
	Object.entries(componentDefinitions).forEach(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}

export { mwc, componentDefinitions };
